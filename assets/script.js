const url = "https://us-central1-ss-devops.cloudfunctions.net/rand?min=1&max=100";

let finalNumber = 0;
let on = "#262A34";
let off = "#DDDDDD";
let hasWon = false;

initGame();

async function initGame() {
  await getNumber();
  document.querySelector("#clues").innerHTML = "";
  document.querySelector("#number-input").value = "";
  document.querySelector("#send-button").disabled = false;
  hasWon = false;
  on = "#262A34";
  clearPreviousNumbers([''])
  clearRestartButton();
}

async function getNumber() {
  try {
    var res = await fetch(url).then((res) => res.json());
    if(res.StatusCode) {
      throw new Error(res.StatusCode);
    } else {
      finalNumber = res.value;
    }
  } catch (error) {
    document.querySelector("#clues").innerHTML = "Ocorreu um erro";
    restartGameButton();
    on = "#bd2130"
    drawNumbers(error.message);
    return Promise.reject(error);
  }
}

async function checkNumber() {
  let input = document.querySelector("#number-input").value;
  if(input > 0) {
    showClues(input);
    drawNumbers(input);
  } else {
    document.querySelector("#clues").innerHTML = "Insira um número positivo";
  }

}

function showClues(input) {

  if(input > finalNumber) {
    document.querySelector("#clues").innerHTML = "Número maior que o esperado";
  } else if(input < finalNumber) {
    document.querySelector("#clues").innerHTML = "Número menor que o esperado";
  } else {
    document.querySelector("#clues").innerHTML = "Número correto";
    document.querySelector("#send-button").disabled = true;
    on = "#1e7e34";
    hasWon = true;
    restartGameButton();
  }
}

function drawNumbers(input) {
  input = parseInt(input, 10); // remove leading zeroes
  input = input.toString(); // convert to string
  let number = input.split(""); // split into array

  clearPreviousNumbers(number);

  number.forEach((element, index) => {
    changeColor(element, index);
  });
}

function clearPreviousNumbers(number) {
  // clears previous numbers
  document.querySelectorAll("#items-box").forEach((display) => {
    display.innerHTML = "";
  });

  // draws new numbers
  for(let i = 0; i < number.length; i++) {
    var element = $(`<div id="number-display-${i}">`
      +'<div id="top-box"></div>'
      +'<div id="bottom-box"></div>'
      +'</div>'
    );

    $("#items-box").append(element);
  }
}

function changeColor(input, index) {
  let numberDisplay = document.querySelector(`#number-display-${index}`);
  let topBox = numberDisplay.querySelector("#top-box").style;
  let bottomBox = numberDisplay.querySelector("#bottom-box").style;

  switch (input) {
    case "0":
      zero(topBox, bottomBox);
      break;
    case "1":
      one(topBox, bottomBox);
      break;
    case "2":
      two(topBox, bottomBox);
      break;
    case "3":
      three(topBox, bottomBox);
      break;
    case "4":
      four(topBox, bottomBox);
      break;
    case "5":
      five(topBox, bottomBox);
      break;
    case "6":
      six(topBox, bottomBox);
      break;
    case "7":
      seven(topBox, bottomBox);
      break;
    case "8":
      eight(topBox, bottomBox);
      break;
    case "9":
      nine(topBox, bottomBox);
      break;
  }
}

let zero = (topBox, bottomBox) => {
  topBox.borderTopColor = on;
  topBox.borderLeftColor = on;
  topBox.borderRightColor = on;

  bottomBox.borderTopColor = off;
  bottomBox.borderLeftColor = on;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = on;
};

let one = (topBox, bottomBox) => {
  topBox.borderTopColor = off;
  topBox.borderLeftColor = off;
  topBox.borderRightColor = on;

  bottomBox.borderTopColor = off;
  bottomBox.borderLeftColor = off;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = off;
};

let two = (topBox, bottomBox) => {
  topBox.borderTopColor = on;
  topBox.borderLeftColor = off;
  topBox.borderRightColor = on;

  bottomBox.borderTopColor = on;
  bottomBox.borderLeftColor = on;
  bottomBox.borderRightColor = off;
  bottomBox.borderBottomColor = on;
};

let three = (topBox, bottomBox) => {
  topBox.borderTopColor = on;
  topBox.borderLeftColor = off;
  topBox.borderRightColor = on;

  bottomBox.borderTopColor = on;
  bottomBox.borderLeftColor = off;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = on;
};

let four = (topBox, bottomBox) => {
  topBox.borderTopColor = off;
  topBox.borderLeftColor = on;
  topBox.borderRightColor = on;

  bottomBox.borderTopColor = on;
  bottomBox.borderLeftColor = off;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = off;
};

let five = (topBox, bottomBox) => {
  topBox.borderTopColor = on;
  topBox.borderLeftColor = on;
  topBox.borderRightColor = off;

  bottomBox.borderTopColor = on;
  bottomBox.borderLeftColor = off;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = on;
};

let six = (topBox, bottomBox) => {
  topBox.borderTopColor = on;
  topBox.borderLeftColor = on;
  topBox.borderRightColor = off;

  bottomBox.borderTopColor = on;
  bottomBox.borderLeftColor = on;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = on;
};

let seven = (topBox, bottomBox) => {
  topBox.borderTopColor = on;
  topBox.borderLeftColor = on;
  topBox.borderRightColor = on;

  bottomBox.borderTopColor = off;
  bottomBox.borderLeftColor = off;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = off;
};

let eight = (topBox, bottomBox) => {
  topBox.borderTopColor = on;
  topBox.borderLeftColor = on;
  topBox.borderRightColor = on;

  bottomBox.borderTopColor = on;
  bottomBox.borderLeftColor = on;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = on;
};

let nine = (topBox, bottomBox) => {
  topBox.borderTopColor = on;
  topBox.borderLeftColor = on;
  topBox.borderRightColor = on;

  bottomBox.borderTopColor = on;
  bottomBox.borderLeftColor = off;
  bottomBox.borderRightColor = on;
  bottomBox.borderBottomColor = on;
};

function restartGameButton() {
  var element = $(`<button id="reset-button" class="btn btn-primary" onclick="initGame()">Reiniciar</button>`);
  $("#restart-game").append(element);
}

function clearRestartButton() {
  $("#reset-button").remove();
}